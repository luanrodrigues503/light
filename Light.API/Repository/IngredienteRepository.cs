﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class IngredienteRepository : IIngredienteRepository
    {

        private readonly AppDBContext _db;

        public IngredienteRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(Ingrediente ingrediente)
        {
            _db.Ingredientes.Add(ingrediente);
            _db.SaveChanges();
        }

        public Ingrediente Find(Guid Id)
        {
            return _db.Ingredientes.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Ingrediente> GetAll(string IdUsuario)
        {
            return _db.Ingredientes.Where(i=> i.IdUsuario.Equals(IdUsuario)).ToList();
        }

        public void Remove(Guid Id)
        {
            var ingrediente = _db.Ingredientes.FirstOrDefault(i => i.Id == Id);
            _db.Ingredientes.Remove(ingrediente);
            _db.SaveChanges();

        }

        public void Update(Ingrediente ingrediente)
        {

            _db.Ingredientes.Update(ingrediente);
            _db.SaveChanges();

        }
    }
}
