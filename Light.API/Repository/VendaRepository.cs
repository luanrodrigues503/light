﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class VendaRepository : IVendaRepository
    {

        private readonly AppDBContext _db;

        public VendaRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(Venda venda)
        {
            _db.Vendas.Add(venda);
            _db.SaveChanges();
        }

        public Venda Find(Guid Id)
        {
            return _db.Vendas.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Venda> GetAll(string IdUsuario)
        {
            return _db.Vendas.Where(i=>i.IdUsuario == IdUsuario).ToList();
        }

        public void RemoveRange(string Id)
        {
            var venda = _db.Vendas.Where(i => i.IdUsuario == Id).ToList();
            _db.Vendas.RemoveRange(venda);
            _db.SaveChanges();

        }

        public void Remove(Guid Id)
        {
            var venda = _db.Vendas.FirstOrDefault(i => i.Id == Id);
            _db.Vendas.RemoveRange(venda);
            _db.SaveChanges();

        }

        public void Update(Venda venda)
        {

            _db.Vendas.Update(venda);
            _db.SaveChanges();

        }
    }
}
