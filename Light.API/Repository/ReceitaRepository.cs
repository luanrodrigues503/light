﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class ReceitaRepository : IReceitaRepository
    {

        private readonly AppDBContext _db;

        public ReceitaRepository(AppDBContext ctx)
        {
            _db = ctx;
        }
         
        public void Add(Receita receita)
        {
            _db.Receitas.Add(receita);
            _db.SaveChanges();
        }

        public Receita Find(Guid Id)
        {
           return _db.Receitas.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Receita> GetAll(string IdUsuario)
        {
            return _db.Receitas.Where(i=>i.IdUsuario== IdUsuario).ToList();
        }

        public void Remove(Guid Id)
        {
            
            var ingredienteReceita = _db.IngredienteReceita.FirstOrDefault(i => i.IdReceita == Id);
            _db.IngredienteReceita.RemoveRange(ingredienteReceita);
            _db.SaveChanges();

            var receita = _db.Receitas.FirstOrDefault(i => i.Id == Id);
            _db.Receitas.Remove(receita);
            _db.SaveChanges();

        }

        public void Update(Receita receita)
        {
            _db.Receitas.Update(receita);
            _db.SaveChanges();

        }
    }
}
