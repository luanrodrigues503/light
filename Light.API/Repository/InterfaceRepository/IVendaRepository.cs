﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface IVendaRepository
    {

        void Add(Venda custo);

        Venda Find(Guid Id);

        void RemoveRange(string Id);

        void Remove(Guid Id);

        void Update(Venda venda);

        IEnumerable<Venda> GetAll(string Id);

    }
}
