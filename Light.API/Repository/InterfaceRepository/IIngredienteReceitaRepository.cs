﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface IIngredienteReceitaRepository
    {

        void Add(IngredienteReceita ingredienteReceita);

        IngredienteReceita Find(Guid Id);

        void Remove(Guid Id);

        void RemoveRange(Guid Id);

        void Update(IngredienteReceita ingredienteReceita);

        IEnumerable<IngredienteReceita> GetAll();

        IEnumerable<IngredienteReceita> GetAllById(Guid Id);

    }
}
