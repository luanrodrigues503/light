﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface ICustoRepository
    {

        void Add(Custo custo);

        Custo Find(Guid Id);

        void Remove(Guid Id);

        void Update(Custo custo);

        IEnumerable<Custo> GetAll(string Id);

    }
}
