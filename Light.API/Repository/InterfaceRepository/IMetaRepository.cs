﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface IMetaRepository
    {

        void Add(Meta meta);

        Meta Find(Guid Id);

        void Remove(Guid Id);

        void Update(Meta meta);

        IEnumerable<Meta> GetAll();

    }
}
