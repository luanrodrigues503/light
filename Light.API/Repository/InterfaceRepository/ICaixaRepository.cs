﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface ICaixaRepository
    {

        void Add(Caixa caixa);

        Caixa Find(Guid Id);

        void Update(Caixa caixa);

        IEnumerable<Caixa> GetAll(string Id);

    }
}
