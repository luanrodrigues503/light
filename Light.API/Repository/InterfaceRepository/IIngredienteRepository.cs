﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface IIngredienteRepository
    {

        void Add(Ingrediente ingrediente);

        Ingrediente Find(Guid Id);

        void Remove(Guid Id);

        void Update(Ingrediente ingrediente);

        IEnumerable<Ingrediente> GetAll(string IdUsuario);

    }
}
