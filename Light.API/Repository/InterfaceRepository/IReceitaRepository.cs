﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Repositorio
{
    public interface IReceitaRepository
    {

        void Add(Receita receita);

        Receita Find(Guid Id);

        void Remove(Guid Id);

        void Update(Receita receita);

        IEnumerable<Receita> GetAll(string Id);

    }
}
