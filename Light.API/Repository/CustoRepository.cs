﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class CustoRepository : ICustoRepository
    {

        private readonly AppDBContext _db;

        public CustoRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(Custo custo)
        {
            _db.Custos.Add(custo);
            _db.SaveChanges();
        }

        public Custo Find(Guid Id)
        {
            return _db.Custos.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Custo> GetAll(string IdUsuario)
        {
            return _db.Custos.Where(i=>i.IdUsuario == IdUsuario).ToList();
        }

        public void Remove(Guid Id)
        {
            var custo = _db.Custos.FirstOrDefault(i => i.Id == Id);
            _db.Custos.Remove(custo);
            _db.SaveChanges();

        }

        public void Update(Custo custo)
        {

            _db.Custos.Update(custo);
            _db.SaveChanges();

        }
    }
}
