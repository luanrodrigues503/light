﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class CaixaRepository : ICaixaRepository
    {

        private readonly AppDBContext _db;

        public CaixaRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(Caixa caixa)
        {
            _db.Caixa.Add(caixa);
            _db.SaveChanges();
        }

        public Caixa Find(Guid Id)
        {
            return _db.Caixa.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Caixa> GetAll(string IdUsuario)
        {
            return _db.Caixa.Where(i=>i.IdUsuario == IdUsuario).OrderByDescending(d=>d.DataFechamento).ToList();
        }


        public void Update(Caixa caixa)
        {

            _db.Caixa.Update(caixa);
            _db.SaveChanges();

        }
    }
}
