﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class MetaRepository : IMetaRepository
    {

        private readonly AppDBContext _db;

        public MetaRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(Meta meta)
        {
            _db.Metas.Add(meta);
            _db.SaveChanges();
        }

        public Meta Find(Guid Id)
        {
            return _db.Metas.FirstOrDefault(i => i.Id == Id);
        }

        public IEnumerable<Meta> GetAll()
        {
            return _db.Metas.ToList();
        }

        public void Remove(Guid Id)
        {
            var meta = _db.Metas.FirstOrDefault(i => i.Id == Id);
            _db.Metas.Remove(meta);
            _db.SaveChanges();

        }

        public void Update(Meta meta)
        {

            _db.Metas.Update(meta);
            _db.SaveChanges();

        }
    }
}
