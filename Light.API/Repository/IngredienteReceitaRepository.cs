﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Data;
using Light.API.Models;

namespace Light.API.Repositorio
{
    public class IngredienteReceitaRepository : IIngredienteReceitaRepository
    {

        private readonly AppDBContext _db;

        public IngredienteReceitaRepository(AppDBContext ctx)
        {
            _db = ctx;
        }

        public void Add(IngredienteReceita ingrediente)
        {
            _db.IngredienteReceita.Add(ingrediente);
            _db.SaveChanges();
        }

        public IngredienteReceita Find(Guid Id)
        {
            return _db.IngredienteReceita.FirstOrDefault(i=>i.Id == Id);
        }

        public IEnumerable<IngredienteReceita> GetAll()
        {
            return _db.IngredienteReceita.ToList();
        }

        public IEnumerable<IngredienteReceita> GetAllById(Guid Id)
        {
            return _db.IngredienteReceita.Where(i => i.IdReceita == Id).ToList();
        }

        public void Remove(Guid Id)
        {
            var ingrediente = _db.IngredienteReceita.FirstOrDefault(i => i.Id == Id);
            _db.IngredienteReceita.Remove(ingrediente);
            _db.SaveChanges();

        }

        public void RemoveRange(Guid id)
        {
            var ingredientes = _db.IngredienteReceita.Where(i => i.IdReceita == id).ToList();
            _db.IngredienteReceita.RemoveRange(ingredientes);
            _db.SaveChanges();
        }

        public void Update(IngredienteReceita ingrediente)
        {

            _db.IngredienteReceita.Update(ingrediente);
            _db.SaveChanges();

        }
    }
}
