﻿using Light.API.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Data
{
    public class AppDBContext : IdentityDbContext<Usuario>
    {
        public AppDBContext(DbContextOptions<AppDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
           
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Receita> Receitas { get; set; }
        public DbSet<Ingrediente> Ingredientes { get; set; }
        public DbSet<IngredienteReceita> IngredienteReceita { get; set; }
        public DbSet<Custo> Custos { get; set; }
        public DbSet<Meta> Metas { get; set; }
        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Caixa> Caixa { get; set; }


    }

  

}
