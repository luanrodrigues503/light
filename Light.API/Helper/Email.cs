﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Light.API.Helper
{

    public class Email
    {

        public static SmtpClient InitServer()
        {
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com")
            {
                Port = 587,
                Credentials =
           new NetworkCredential("luanrodrigues503@gmail.com", "sua-senha-aqui"),
                EnableSsl = true,
            };

            return SmtpServer;
        }


        public static bool SendNovaSenha(string userEmail, int novasenha)
        {
            try
            {
                MailMessage mail = new MailMessage
                {
                    From = new MailAddress("luanrodrigues503@gmail.com"),
                    Subject = "Recuperar Senha",
                    Body = $"Olá, sua nova senha é @{novasenha}, " +
                    $"ao entrar no sistema você poderá redefinir esta senha!"
                };

                mail.To.Add(userEmail);

                var servidor = InitServer();

                servidor.Send(mail);

                return true;

            }
            catch (Exception)
            {
                throw new Exception();
            }

        }
    }
}
