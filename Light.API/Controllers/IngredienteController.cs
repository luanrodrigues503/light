﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/ingrediente")]

    [Authorize("Bearer")]
    public class IngredienteController : Controller
    {
        private readonly IIngredienteRepository _repositorio;

        public IngredienteController(IIngredienteRepository repo)
        {
            _repositorio = repo;
        }


        [AllowAnonymous]
        [HttpPost("/ingrediente/getAll")]
        public IActionResult GetAll([FromBody] UsuarioVM user)
        {
            return Ok(_repositorio.GetAll(user.Id));
        }


        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var ingrediente = _repositorio.Find(Id);

            if (ingrediente == null)
                return NotFound();

            return Ok(ingrediente);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Ingrediente ingrediente)
        {
            if (ingrediente == null)
                return BadRequest();


            ingrediente.PrecoUnidade = ingrediente.Preco / ingrediente.Quantidade;

            _repositorio.Add(ingrediente);

            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] Ingrediente ingrediente)
        {
            if (ingrediente.Id == null)
                return BadRequest();

            var _ingrediente = _repositorio.Find(ingrediente.Id);

            if (_ingrediente == null)
                return NotFound();

            _ingrediente.Nome = ingrediente.Nome;
            _ingrediente.Preco = ingrediente.Preco;
            _ingrediente.Quantidade = ingrediente.Quantidade;
            _ingrediente.UnidadeMedida = ingrediente.UnidadeMedida;
            _ingrediente.PrecoUnidade = ingrediente.Preco / ingrediente.Quantidade;

            _repositorio.Update(_ingrediente);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid Id)
        {

            var _ingrediente = _repositorio.Find(Id);

            if (_ingrediente == null)
                return NotFound();

            try
            {
                _repositorio.Remove(Id);

            }
            catch (Exception ex)
            {
                if (ex.Source == "Microsoft.EntityFrameworkCore.Relational")
                    return BadRequest("Este ingrediente está sendo utilizado em uma de suas receitas");

                return BadRequest(ex);
            }


            return Ok();
        }

    }
}