﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/caixa")]
    [Authorize("Bearer")]
    public class CaixaController : Controller
    {
        private readonly ICaixaRepository _repositorio;
        private readonly IVendaRepository _vendaRepositorio;
        private readonly ICustoRepository _custoRepository;

        public CaixaController(ICaixaRepository repo,
            IVendaRepository venda,
            ICustoRepository custo)
        {
            _repositorio = repo;
            _vendaRepositorio = venda;
            _custoRepository = custo;
        }


        [HttpPost("/caixa/getAll")]
        public IActionResult GetAll([FromBody] UsuarioVM user)
        {
            var caixas = _repositorio.GetAll(user.Id);
            var valorTotal = caixas.Sum(i => i.Saldo);

            return Ok(new
            {
                listaCaixas = caixas,
                totalSaldo = valorTotal
            });
        }


        [HttpPost("/caixa/pesquisarData")]
        public IActionResult PesquisarData([FromBody] CaixaVM vm)
        {

            if (vm.DataInicio.Date > vm.DataFim.Date)
                return BadRequest(new { Message = "A data final precisa ser maior que a data inicial" });


            var caixas = _repositorio.GetAll(vm.IdUsuario)
                .Where(i => i.DataFechamento.Date >= vm.DataInicio.Date && i.DataFechamento.Date <= vm.DataFim.Date)
                .OrderBy(i => i.DataFechamento);


            var valorTotal = caixas.Sum(i => i.Saldo);

            return Ok(new
            {
                listaCaixas = caixas,
                totalSaldo = valorTotal,
            });
        }

        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var caixa = _repositorio.Find(Id);

            if (caixa == null)
                return NotFound();

            return Ok(caixa);
        }

        [HttpPost]
        public IActionResult Create([FromBody] CaixaVM caixa)
        {

            caixa.DataFechamento = DateTime.Now;


            var caixas = _repositorio.GetAll(caixa.IdUsuario);

            var custos = _custoRepository.GetAll(caixa.IdUsuario);

            var totalGastos = custos.Sum(i => i.Valor);

            var _caixa = CaixaVM.ToModel(caixa);

            var diasMes = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);

            if (caixas.Any(i => i.DataFechamento.Date == _caixa.DataFechamento.Date))
            {

                var caixaExistente = caixas.FirstOrDefault(i => i.DataFechamento.Day == _caixa.DataFechamento.Day);



                caixaExistente.TotalCaixa += _caixa.TotalCaixa;
                caixaExistente.CustoReceitaTotal += _caixa.CustoReceitaTotal;
                caixaExistente.Saldo += _caixa.TotalCaixa - (_caixa.CustoReceitaTotal + _caixa.CustoEstabelecimento);


                _repositorio.Update(caixaExistente);
            }
            else
            {

                _caixa.CustoEstabelecimento = (totalGastos / diasMes);
                _caixa.Saldo = _caixa.TotalCaixa - (_caixa.CustoReceitaTotal + _caixa.CustoEstabelecimento);

                _repositorio.Add(_caixa);

            }


            _vendaRepositorio.RemoveRange(_caixa.IdUsuario);


            return Ok();
        }





    }
}