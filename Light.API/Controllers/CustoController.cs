﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/custo")]
    [Authorize("Bearer")]
    public class CustoController : Controller
    {
        private readonly ICustoRepository _repositorio;

        public CustoController(ICustoRepository repo)
        {
            _repositorio = repo;
        }


        [HttpPost("/custo/getAll")]
        public IActionResult GetAll([FromBody] UsuarioVM user)
        {
            return Ok(_repositorio.GetAll(user.Id));
        }

        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var custo = _repositorio.Find(Id);

            if (custo == null)
                return NotFound();

            return Ok(custo);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Custo custo)
        {
            if (custo == null)
                return BadRequest();

            _repositorio.Add(custo);

            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] Custo custo)
        {
            if (custo.Id == null)
                return BadRequest();

            var _custo = _repositorio.Find(custo.Id);

            if (_custo == null)
                return NotFound();

            _custo.Nome = custo.Nome;
            _custo.Valor = custo.Valor;


            _repositorio.Update(_custo);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid Id)
        {

            var _custo = _repositorio.Find(Id);

            if (_custo == null)
                return NotFound();

            _repositorio.Remove(Id);

            return Ok();
        }

        [HttpPost("/custo/ResumoCustos")]
        public IActionResult ResumoCustos([FromBody] UsuarioVM user)
        {
            var custos = _repositorio.GetAll(user.Id);

            if (custos.Count() == 0)
                return BadRequest(new { Message = "sem gastos cadastrados"});

            var maiorValor = custos.Max(i => i.Valor);

            var custosVM = new ResumoCustosVM()
            {

                CustoTotal = custos.Sum(i => i.Valor),
                MaiorGasto = custos.Where(i => i.Valor == maiorValor).FirstOrDefault()

            };

            return Ok(custosVM);
        }

    }
}