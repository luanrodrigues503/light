﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/venda")]
    [Authorize("Bearer")]
    public class VendaController : Controller
    {
        private readonly IVendaRepository _repositorio;
        private readonly IReceitaRepository _receitaRepo;

        public VendaController(IVendaRepository repo, IReceitaRepository receitaRepo)
        {
            _repositorio = repo;
            _receitaRepo = receitaRepo;
        }


        [HttpPost("/venda/getAll")]
        public IActionResult GetAll([FromBody] UsuarioVM user)
        {
            var vendas = _repositorio.GetAll(user.Id);

            var vendasVm = new List<VendaVM>();

            var totalVenda = 0m;

            foreach (var item in vendas)
            {
                var receita = _receitaRepo.Find(item.IdReceita);
                var venda = new VendaVM()
                {
                    Id = item.Id,
                    NomeReceita = receita.Nome,
                    ValorVenda = receita.Preco * item.QuantidadeVendida,
                    QuantidadeVendida = item.QuantidadeVendida,
                };

                totalVenda += venda.ValorVenda;

                vendasVm.Add(venda);

            }

            return Ok(new
            {
                vendas = vendasVm,
                totalVendas = totalVenda
            });
        }

        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var venda = _repositorio.Find(Id);

            if (venda == null)
                return NotFound();

            return Ok(venda);
        }


        [HttpPost]
        public IActionResult Create([FromBody] List<Venda> vendasVM)
        {
            var vendidos = _repositorio.GetAll(vendasVM.First().IdUsuario);
            if (vendidos.Count() < 1)
            {
                foreach (var item in vendasVM)
                {
                    if (item.QuantidadeVendida > 0)
                        _repositorio.Add(item);
                }
            }
            else
            {

                foreach (var item in vendasVM)
                {
                    var vendido = vendidos.FirstOrDefault(i => i.IdReceita == item.IdReceita);
                    if (vendido != null)
                    {
                        vendido.QuantidadeVendida += item.QuantidadeVendida;
                        _repositorio.Update(vendido);

                    }
                    else
                    {
                        if (item.QuantidadeVendida > 0)
                            _repositorio.Add(item);
                    }

                }
            }

            return Ok();

        }

        [HttpPut]
        public IActionResult Update([FromBody] Venda venda)
        {
            if (venda.Id == null)
                return BadRequest();

            var _venda = _repositorio.Find(venda.Id);

            if (_venda == null)
                return NotFound();

            _venda.QuantidadeVendida = venda.QuantidadeVendida;

            _repositorio.Update(_venda);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid Id)
        {

            var _venda = _repositorio.Find(Id);

            if (_venda == null)
                return NotFound();

            _repositorio.Remove(Id);

            return Ok();
        }



    }
}