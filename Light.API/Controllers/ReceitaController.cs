﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/receita")]
    [Authorize("Bearer")]
    public class ReceitaController : Controller
    {
        private readonly IReceitaRepository _repository;
        private readonly IIngredienteReceitaRepository _ingReceitaRepository;
        private readonly IIngredienteRepository _ingRepository;
        private readonly IMetaRepository _metaRepository;
        private readonly IVendaRepository _vendaRepository;

        public ReceitaController(IReceitaRepository repo,
            IIngredienteReceitaRepository ingRec,
            IIngredienteRepository ing,
             IVendaRepository venda,
            IMetaRepository meta)
        {
            _repository = repo;
            _ingReceitaRepository = ingRec;
            _ingRepository = ing;
            _metaRepository = meta;
            _vendaRepository = venda;
        }


        [HttpPost("/receita/GetAll")]
        public IActionResult GetAll([FromBody] UsuarioVM user)
        {
            IEnumerable<ReceitaVM> receitaVM = _repository.GetAll(user.Id).Select(item => ReceitaVM.FromModel(item));

            var receitaLista = new List<ReceitaVM>();

            foreach (var receita in receitaVM)
            {
                var listaIngredientes = _ingReceitaRepository.GetAllById(receita.Id).ToList();

                foreach (var item in listaIngredientes)
                {
                    var ingrediente = _ingRepository.Find(item.IdIngrediente);
                    receita.PrecoCusto += item.QuantidadeUtilizada * ingrediente.PrecoUnidade;
                }


                var lucro = receita.Preco - receita.PrecoCusto;

                receita.Lucro = Math.Round(((lucro / receita.Preco) * 100), 2);

                receita.CustoPorcao = Math.Round((receita.PrecoCusto / receita.NumPorcoes), 2);

                receita.PrecoCusto = Math.Round(receita.PrecoCusto, 2);

                receitaLista.Add(receita);
            }

            receitaVM = receitaLista.AsEnumerable();

            var custosDia = ResumoCustoDia(user.Id);

            return Ok(new
            {
                receitas = receitaVM,
                custoPorDia = custosDia,
            });
        }

        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var receita = _repository.Find(Id);

            if (receita == null)
                return NotFound();

            var receitaVM = ReceitaVM.FromModel(receita);

            var listaIngredientes = _ingReceitaRepository.GetAllById(receita.Id).ToList();

            foreach (var item in listaIngredientes)
            {
                var ingrediente = _ingRepository.Find(item.IdIngrediente);
                ingrediente.Quantidade = item.QuantidadeUtilizada;
                receitaVM.ListaIngredientes.Add(ingrediente);
            }

            return Ok(receitaVM);
        }

        [HttpPost]
        public IActionResult Create([FromBody] ReceitaVM vm)
        {
            if (vm == null)
                return BadRequest();

            Receita receita = ReceitaVM.ToModel(vm);


            _repository.Add(receita);

            vm.ListaIngredientes.ForEach(item =>
            {
                item = ConveterUnidades(item);

                var ingredienteReceita = new IngredienteReceita
                {
                    Id = Guid.NewGuid(),
                    IdIngrediente = item.Id,
                    IdReceita = receita.Id,
                    QuantidadeUtilizada = item.Quantidade,
                };

                _ingReceitaRepository.Add(ingredienteReceita);

            });


            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] ReceitaVM vm)
        {
            if (vm.Id == null)
                return BadRequest();

            var receita = _repository.Find(vm.Id);

            if (receita == null)
                return NotFound();

            receita.Nome = vm.Nome;
            receita.Preco = vm.Preco;
            receita.NumPorcoes = vm.NumPorcoes;

            _ingReceitaRepository.RemoveRange(vm.Id);

            vm.ListaIngredientes.ForEach(item =>
            {
                item = ConveterUnidades(item);


                var ingredienteReceita = new IngredienteReceita
                {
                    Id = Guid.NewGuid(),
                    IdIngrediente = item.Id,
                    IdReceita = vm.Id,
                    QuantidadeUtilizada = item.Quantidade
                };


                _ingReceitaRepository.Add(ingredienteReceita);
            });


            _repository.Update(receita);

            return Ok();
        }

        [HttpPut("/receita/UpdateAtivo")]
        public IActionResult UpdateAtivo([FromBody] ReceitaVM vm)
        {
            if (vm.Id == null)
                return BadRequest();

            var receita = _repository.Find(vm.Id);

            if (receita == null)
                return NotFound();

            receita.Ativo = vm.Ativo;

            _repository.Update(receita);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid Id)
        {

            var _receita = _repository.Find(Id);

            if (_receita == null)
                return NotFound();

            _repository.Remove(Id);

            return Ok();
        }

        [HttpPost("/receita/resumoReceita")]
        public IActionResult ResumoReceita([FromBody] UsuarioVM user)
        {
            var receitas = _repository.GetAll(user.Id)?.Where(i => i.Ativo);

            var vm = new ResumoReceitaVM();

            foreach (var receita in receitas)
            {

                var listaIngredientes = _ingReceitaRepository.GetAllById(receita.Id).ToList();

                var precoCusto = 0m;

                foreach (var item in listaIngredientes)
                {
                    var ingrediente = _ingRepository.Find(item.IdIngrediente);
                    precoCusto += item.QuantidadeUtilizada * ingrediente.PrecoUnidade;
                }

                vm.CustoTotal += precoCusto;
            }

            return Ok(vm);
        }

        public decimal ResumoCustoDia(string idUsuario)
        {
            var vendas = _vendaRepository.GetAll(idUsuario);
            var custoTotal = 0m;

            foreach (var venda in vendas)
            {

                var listaIngredientes = _ingReceitaRepository.GetAllById(venda.IdReceita).ToList();

                var precoCusto = 0m;

                foreach (var item in listaIngredientes)
                {
                    var ingrediente = _ingRepository.Find(item.IdIngrediente);
                    precoCusto += item.QuantidadeUtilizada * ingrediente.PrecoUnidade;
                }

                custoTotal += precoCusto * venda.QuantidadeVendida;
            }

            return custoTotal;

        }

        public Ingrediente ConveterUnidades(Ingrediente item)
        {

            var ingrediente = _ingRepository.Find(item.Id);

            if (item.UnidadeMedida == "G" && ingrediente.UnidadeMedida == "KG")
            {
                item.Quantidade = item.Quantidade / 1000;
            }

            if (item.UnidadeMedida == "KG" && ingrediente.UnidadeMedida == "G")
            {
                item.Quantidade = item.Quantidade * 1000;
            }

            if (item.UnidadeMedida == "L" && ingrediente.UnidadeMedida == "ML")
            {
                item.Quantidade = item.Quantidade * 1000;
            }

            if (item.UnidadeMedida == "ML" && ingrediente.UnidadeMedida == "L")
            {
                item.Quantidade = item.Quantidade / 1000;
            }


            return item;
        }

    }
}