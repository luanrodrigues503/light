﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/meta")]
    [Authorize("Bearer")]
    public class MetaController : Controller
    {
        private readonly IMetaRepository _repositorio;
        private readonly IReceitaRepository _recRepositorio;

        public MetaController(IMetaRepository repo, IReceitaRepository recRepo)
        {
            _repositorio = repo;
            _recRepositorio = recRepo;
        }

        [HttpGet]
        public IEnumerable<Meta> GetAll()
        {
            return _repositorio.GetAll();
        }

        [HttpGet("{Id}")]
        public IActionResult GetById(Guid Id)
        {
            var meta = _repositorio.Find(Id);

            if (meta == null)
                return NotFound();

            return Ok(meta);
        }

        [HttpPost]
        public IActionResult Create([FromBody] Meta meta)
        {
            if (meta == null)
                return BadRequest();

            _repositorio.Add(meta);

            return Ok();
        }

        [HttpPut]
        public IActionResult Update([FromBody] Meta meta)
        {
            if (meta.Id == null)
                return BadRequest();

            var _meta = _repositorio.Find(meta.Id);

            if (_meta == null)
                return NotFound();

            _meta.Nome = meta.Nome;
            _meta.Valor = meta.Valor;


            _repositorio.Update(_meta);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid Id)
        {

            var _meta = _repositorio.Find(Id);

            if (_meta == null)
                return NotFound();

            _repositorio.Remove(Id);

            return Ok();
        }

        [HttpPost("/meta/resumoMeta")]
        public IActionResult ResumoMeta([FromBody] UsuarioVM user)
        {
            var receitas = _recRepositorio.GetAll(user.Id)?.Where(i => i.Ativo);

            var vm = new ResumoMetaVM();

            var meta = _repositorio.GetAll().FirstOrDefault();
            if (meta == null)
                meta = new Meta() { Valor = 0};

            vm.Nome = meta.Nome;
            vm.Valor = meta.Valor;
            var baterMeta = 0m;

            var listaMeta = new List<Receita>();

            while (baterMeta < meta.Valor)
            {
                foreach (var item in receitas)
                {

                    if (listaMeta.FirstOrDefault(receita => receita.Preco > meta.Valor) != null)
                        break;

                    if (baterMeta < meta.Valor)
                    {
                        baterMeta += item.Preco;
                        listaMeta.Add(item);

                    }

                }

            }

            vm.ListaReceitasMeta = new List<ResumoMetaVM>();

            var resultadoMeta = listaMeta.GroupBy(x => x.Id)
                .Select(x => new ResumoMetaVM
                {
                    QuantidadeReceitasBaterMeta = x.Count(),
                    NomesReceitasBaterMeta = x.First().Nome,
                    IdReceita = x.First().Id,
                    PrecoReceita = x.First().Preco
                });

            foreach (var item in resultadoMeta)
            {
                vm.ListaReceitasMeta.Add(item);
            }

            return Ok(vm);
        }
    }
}