﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Light.API.Helper;
using Light.API.JWT;
using Light.API.Models;
using Light.API.Repositorio;
using Light.API.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("/usuario")]

    public class UsuarioController : Controller
    {
        private readonly SignInManager<Usuario> _sign;
        private readonly UserManager<Usuario> _user;

        public UsuarioController(SignInManager<Usuario> _signManager, UserManager<Usuario> _userManager)
        {
            _sign = _signManager;
            _user = _userManager;

        }

        [HttpPost("/usuario/login")]
        public async Task<IActionResult> Login([FromBody] LoginVM vm,
            [FromServices] SigningConfigurations signingConfigurations)
        {
            var user = await _user.FindByEmailAsync(vm.Email);

            if (user == null)
                return BadRequest(new { erro = "Usuário inexistente" });

            var result = await _sign.PasswordSignInAsync(vm.Email, vm.Senha, isPersistent: false, lockoutOnFailure: false);


            var userVM = new UsuarioVM
            {
                Token = TokenJWT.NewTokenFor(vm.Email, signingConfigurations),
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email
            };

            if (result.Succeeded)
                return Ok(userVM);


            return BadRequest();

        }

        [HttpGet("/usuario/logout")]
        public async Task<IActionResult> LogOut()
        {
            await _sign.SignOutAsync();
            return Ok();
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] CadastroUsuarioVM vm,
            [FromServices] SigningConfigurations signingConfigurations)
        {

            var newUser = new Usuario { UserName = vm.Email, Email = vm.Email };

            var result = await _user.CreateAsync(newUser, vm.Senha);


            if (result.Succeeded)
                await _sign.SignInAsync(newUser, isPersistent: false);

            if (result.Errors.FirstOrDefault(i => i.Code == "DuplicateEmail") != null)
                return BadRequest(new { erro = "Este email já esta cadastrado" });

            var user = await _user.FindByEmailAsync(vm.Email);

            var userVM = new UsuarioVM
            {
                Token = TokenJWT.NewTokenFor(vm.Email, signingConfigurations),
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email
            };

            return Ok(userVM);

        }

        [HttpPost("/usuario/trocarSenha")]
        public async Task<IActionResult> TrocarSenha([FromBody] TrocarSenhaVM vm)
        {

            var user = await _user.FindByIdAsync(vm.Id);

            var token = await _user.GeneratePasswordResetTokenAsync(user);

            var result = await _user.ResetPasswordAsync(user, token, vm.Senha);

            if (result.Succeeded)
                return Ok();

            return BadRequest();
        }

        [HttpPost("/usuario/recuperarSenha")]
        public async Task<IActionResult> RecuperarSenha([FromBody] UsuarioVM vm)
        {

            var user = await _user.FindByEmailAsync(vm.Email);
            if (user == null)
                return BadRequest("usuário não encontrado");

            var token = await _user.GeneratePasswordResetTokenAsync(user);

            var random = new Random();
            var novasenha = random.Next(1000, 9000);

            var result = await _user.ResetPasswordAsync(user, token, $"@{novasenha}");

            if (!result.Succeeded)
                return BadRequest();

            if (!Email.SendNovaSenha(user.Email, novasenha))
                return BadRequest();

            return Ok();

        }

    }
}