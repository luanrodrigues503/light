﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Light.API.Models;
using Light.API.Repositorio;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Light.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Home")]
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Get() {
            return Ok("API LIGHT - " + DateTime.Now);
        }

    }
}