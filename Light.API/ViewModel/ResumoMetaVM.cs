﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class ResumoMetaVM
    {
        public string IdUsuario { get; set; }
        public Guid IdReceita { get; set; }
        public string Nome { get; set; }
        public decimal Valor { get; set; }
        public decimal PrecoReceita { get; set; }
        public int QuantidadeReceitasBaterMeta { get; set; }
        public string NomesReceitasBaterMeta { get; set; }
        public List<ResumoMetaVM> ListaReceitasMeta { get; set; }



    }
}
