﻿using AutoMapper;
using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class CaixaVM
    {
        public Guid Id { get; set; }
        public decimal CustoReceitaTotal { get; set; }
        public decimal TotalCaixa { get; set; }
        public decimal CustoEstabelecimento { get; set; }
        public decimal Saldo { get; set; }
        public DateTime DataFechamento { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string IdUsuario { get; set; }

        public static Caixa ToModel(CaixaVM vm)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<CaixaVM, Caixa>();
            });

            var model = Mapper.Map<CaixaVM, Caixa>(vm);
            Mapper.Reset();
            return model;
        }
    }
}
