﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class CadastroUsuarioVM
    {
        public string Email { get; set; }
        public string Senha { get; set; }
    }
}
