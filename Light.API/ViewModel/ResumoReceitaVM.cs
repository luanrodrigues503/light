﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class ResumoReceitaVM
    {
        public Receita Receita { get; set; }
        public decimal CustoTotal { get; set; }
    }
}
