﻿using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class ResumoCustosVM
    {
        public decimal CustoTotal { get; set; }
        public Custo MaiorGasto { get; set; }
    }
}
