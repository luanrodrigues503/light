﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class TrocarSenhaVM
    {
        public string Id { get; set; }
        public string Senha { get; set; }
    }
}
