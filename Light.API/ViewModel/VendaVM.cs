﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class VendaVM
    {
        public Guid Id { get; set; }
        public string IdUsuario { get; set; }
        public Guid IdReceita { get; set; }
        public string NomeReceita { get; set; }
        public decimal ValorVenda { get; set; }
        public decimal QuantidadeVendida { get; set; }

    }
}
