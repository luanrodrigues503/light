﻿using AutoMapper;
using Light.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.ViewModel
{
    public class ReceitaVM
    {
        public Guid Id { get; set; }
        public Guid IdUsuario { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public decimal PrecoCusto { get; set; }
        public decimal Lucro { get; set; }
        public decimal NumPorcoes { get; set; }
        public decimal CustoPorcao { get; set; }
        public bool Ativo { get; set; }
        public List<Ingrediente> ListaIngredientes { get; set; }

      
        public static Receita ToModel(ReceitaVM vm) {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<ReceitaVM, Receita>();
            });

            var model = Mapper.Map<ReceitaVM, Receita>(vm);
            Mapper.Reset();
            return model;
        }

        public static ReceitaVM FromModel(Receita model)
        {
            Mapper.Initialize(cfg => {
                cfg.CreateMap<Receita, ReceitaVM>();
            });

            var vm = Mapper.Map<Receita, ReceitaVM>(model);
            vm.ListaIngredientes = new List<Ingrediente>();


            Mapper.Reset();
            return vm;
        }

    }
}
