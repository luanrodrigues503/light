﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Models
{
    public class Caixa
    {
        public Guid Id { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal CustoReceitaTotal { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal CustoEstabelecimento { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal Saldo { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal TotalCaixa { get; set; }
        public DateTime DataFechamento { get; set; }
        public string IdUsuario { get; set; }
    }
}
