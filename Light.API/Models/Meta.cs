﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Models
{
    public class Meta
    {
        public Guid Id { get; set; }
        public string IdUsuario { get; set; }
        public string Nome { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal Valor { get; set; }
    }
}
