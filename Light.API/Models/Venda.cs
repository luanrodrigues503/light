﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Models
{
    public class Venda
    {
        public Guid Id { get; set; }
        public Guid IdReceita { get; set; }
        public string IdUsuario { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal QuantidadeVendida { get; set; }
    }
}
