﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Light.API.Models
{
    public class IngredienteReceita
    {

        public Guid Id { get; set; }
        public Guid IdIngrediente { get; set; }
        public Guid IdReceita { get; set; }
        [Column(TypeName = "decimal(10,2)")]
        public decimal QuantidadeUtilizada { get; set; }
    }
}
