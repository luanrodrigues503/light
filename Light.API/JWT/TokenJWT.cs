﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Light.API.JWT
{
    public class TokenJWT
    {

        public static string NewTokenFor(string Email, SigningConfigurations signingConfigurations) {

            ClaimsIdentity identity = new ClaimsIdentity(
                        new GenericIdentity(Email, "Login"),
                        new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, Email)
                        }
                    );

            DateTime dataCriacao = DateTime.Now;
            DateTime dataExpiracao = dataCriacao +
                TimeSpan.FromDays(3);

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = "Teste.Securiry.Bearer",
                Audience = "Teste.Securiry.Bearer",
                SigningCredentials = signingConfigurations.SigningCredentials,
                Subject = identity,
                NotBefore = dataCriacao,
                Expires = dataExpiracao
            });


            var token = handler.WriteToken(securityToken);

            return token;
        }

    }
}
