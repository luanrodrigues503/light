// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VeeValidate from 'vee-validate'
import messages from 'vee-validate/dist/locale/pt_BR'
import Toasted from 'vue-toasted'
import Chart from 'chart.js'
import VueChartkick from 'vue-chartkick'

import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
import datePicker from 'vue-bootstrap-datetimepicker';
Vue.use(datePicker);

Vue.use(VueResource);
Vue.http.interceptors.push((request, next) => {

  var user = JSON.parse(localStorage.getItem("user"));
  if(!user)
    user = {id: "", token:''}

  request.headers.set('Authorization', 'Bearer ' + user.token)
  next();
});



Vue.use(VeeValidate, {
  locale: 'pt_BR',
  dictionary: {
    pt_BR: messages
  },
});

Vue.use(VueChartkick, {adapter: Chart});

Vue.use(Toasted);

Vue.config.productionTip = false

import './assets/jquery/jquery.js'
import './assets/jquery-ui/jquery-ui.js'
import './assets/jquery-ui/jquery-ui.css'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import './assets/css/style.css'


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
