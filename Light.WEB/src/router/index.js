import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login/Login'
import Home from '@/components/Home/Home'
import ListaIngredientes from '@/components/Ingrediente/ListaIngredientes'
import ListaReceitas from '@/components/Receita/ListaReceitas'
import NovaReceita from '@/components/Receita/NovaReceita'
import ImpressaoReceita from '@/components/Receita/ImpressaoReceita'
import NovoIngrediente from '@/components/Ingrediente/NovoIngrediente'
import AnaliseCusto from '@/components/AnaliseCusto/AnaliseCusto'
import Cadastro from '@/components/Login/Cadastro'
import CadastroCusto from '@/components/Custo/CadastroCusto'
import RecuperarSenha from '@/components/Login/RecuperarSenha'
import Execucao from '@/components/Execucao/Execucao'

Vue.use(Router)

const routes = [{
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/cadastro',
    name: 'Cadastro',
    component: Cadastro,

  },
  {
    path: '/home',
    name: 'Home',
    component: Home,

  },
  {
    path: '/lista-receitas',
    name: 'listaReceitas',
    component: ListaReceitas
  },
  {
    path: '/lista-ingredientes/',
    name: 'listaIngredientes',
    component: ListaIngredientes
  },
  {
    path: '/nova-receita',
    name: 'novaReceita',
    component: NovaReceita
  },
  {
    path: '/nova-receita/:id',
    name: 'novaReceitaId',
    component: NovaReceita
  },
  {
    path: '/impressao-receita/:id',
    name: 'impressaoReceita',
    component: ImpressaoReceita
  },
  {
    path: '/novo-ingrediente',
    name: 'novoIngrediente',
    component: NovoIngrediente
  },
  {
    path: '/novo-ingrediente/:id',
    name: 'novoIngredienteId',
    component: NovoIngrediente
  },
  {
    path: '/analise-custo',
    name: 'analiseCusto',
    component: AnaliseCusto
  },
  {
    path: '/cadastro-custos',
    name: 'CadastroCusto',
    component: CadastroCusto
  },
  {
    path: '/cadastro-custos/:id',
    name: 'custosId',
    component: CadastroCusto
  },
  {
    path: '/recuperarSenha',
    name: 'RecuperarSenha',
    component: RecuperarSenha
  },
  {
    path: '/execucao',
    name: 'Execucao',
    component: Execucao
  },
];

const router = new Router({
  mode: 'history',
  routes
});

router.afterEach((to, from, next) => {

  if (localStorage.getItem("user") || to.path == '/cadastro' || to.path == '/recuperarSenha')
    return;
  else {

    router.push({
      path: '/'
    });

    if (to.path == '/lista-receitas')
      Vue.toasted.error("Efetue o login para utilizar Lista de Receitas!").goAway(5000);

    if (to.path == '/lista-ingredientes')
      Vue.toasted.error("Efetue o login para utilizar Lista de Ingredientes!").goAway(5000);

    if (to.path == '/analise-custo')
      Vue.toasted.error("Efetue o login para utilizar Análise de custo!").goAway(5000);

    if (to.path == '/cadastro-custos')
      Vue.toasted.error("Efetue o login para utilizar cadastro de custos!").goAway(5000);

    if (to.path == '/nova-receita')
      Vue.toasted.error("Efetue o login para cadastrar uma nova receita!").goAway(5000);

    if (to.path == '/novo-ingrediente')
      Vue.toasted.error("Efetue o login para cadastrar um novo ingrediente!").goAway(5000);

    if (to.path == '/execucao')
      Vue.toasted.error("Efetue o login para executar!").goAway(5000);

  }


});

export default router;
