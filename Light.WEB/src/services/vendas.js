import Vue from "vue";
import user from "./usuario";

export default {
  buscar: function () {
    var usuario = user.body();

    return Vue.http.post("http://localhost:5000/venda/getall", usuario);
  },
  atualizar: function (venda) {
    return Vue.http.put("http://localhost:5000/venda/", venda);
  },
  cadastro: function (lista) {

    var usuario = user.body();
    var listaVendidos = []

    lista.forEach(item => {
      var model = {
        id: this.guid(),
        idUsuario: usuario.id,
        idReceita: item.id,
        quantidadeVendida: item.quantidadeVendida
      }
      listaVendidos.push(model);

    });


    return Vue.http.post("http://localhost:5000/venda/", listaVendidos);
  },
  excluir: function (id) {
    return Vue.http.delete("http://localhost:5000/venda/" + id);
  },
  buscarPorId: function (id) {
    return Vue.http.get("http://localhost:5000/venda/" + id);
  },


  guid: function () {
    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
  },

};
