import Vue from "vue";
import user from "./usuario";

export default {
  buscar: function () {
    var usuario = user.body();

    return Vue.http.post("http://localhost:5000/caixa/getall", usuario);
  },

  buscarPorData(inicio, fim){
    var usuario = user.body();

    var model ={
      IdUsuario: usuario.id,
      DataInicio: inicio,
      DataFim: fim

    }

    return Vue.http.post("http://localhost:5000/caixa/pesquisarData", model);
  },

  cadastro: function (caixa) {

    var usuario = user.body();

    caixa.idUsuario = usuario.id;
    caixa.id = this.guid();

    return Vue.http.post("http://localhost:5000/caixa/", caixa);
  },


  guid: function () {
    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
  },

};
