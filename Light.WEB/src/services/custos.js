import Vue from "vue";
import user from "./usuario";

export default {
  buscar: function () {
    var usuario = user.body();

    return Vue.http.post("http://localhost:5000/custo/getall", usuario);
  },
  atualizar: function (custo) {
    return Vue.http.put("http://localhost:5000/custo/", custo);
  },
  cadastro: function (custo) {

    var usuario = user.body();
    custo.idUsuario = usuario.id;
    custo.id = this.guid();

    return Vue.http.post("http://localhost:5000/custo/", custo);
  },
  excluir: function (id) {
    return Vue.http.delete("http://localhost:5000/custo/"+id);
  },
  buscarPorId: function (id) {
    return Vue.http.get("http://localhost:5000/custo/"+id);
  },

  resumoCustos: function(){
    var usuario = user.body();
    return Vue.http.post("http://localhost:5000/custo/ResumoCustos", usuario);
  },
  guid: function () {
    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
  },

};
