import Vue from "vue";

export default {

  trocarSenha: function (user) {

    return Vue.http.post("http://localhost:5000/usuario/trocarSenha", user);

  },

  login: function (user) {

    return Vue.http.post("http://localhost:5000/usuario/Login", user);

  },

  logout: function () {

    return Vue.http.get("http://localhost:5000/usuario/logout");

  },

  cadastro: function (user) {
    return Vue.http.post("http://localhost:5000/usuario/", user);

  },

  recuperar: function (user) {
    return Vue.http.post("http://localhost:5000/usuario/recuperarSenha", user);

  },

  body: function() {

    var user = JSON.parse(localStorage.getItem("user"));
    if(!user)
      user = {id: "", token:''}

    // Vue.http.headers.common['Authorization'] = 'Bearer ' + user.token;

    return user;

  },

};
