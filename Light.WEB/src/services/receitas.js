import Vue from "vue";
import user from './usuario'


export default {
  buscar: function () {

    var usuario = user.body();
    return Vue.http.post("http://localhost:5000/receita/getAll", usuario);
  },

  atualizar: function (receita) {
    return Vue.http.put("http://localhost:5000/receita/", receita);
  },

  atualizarAtivo: function (model) {

    return Vue.http.put("http://localhost:5000/receita/UpdateAtivo", model);
  },

  excluir: function (id) {
    return Vue.http.delete("http://localhost:5000/receita/"+id);
  },
  buscarPorId: function (id) {
    return Vue.http.get("http://localhost:5000/receita/"+id);
  },
  cadastro: function (receita) {
    receita.id = this.guid();

    var usuario = user.body();
    receita.idUsuario = usuario.id;

    return Vue.http.post("http://localhost:5000/receita/", receita);
  },
  resumoReceita: function(){

    var usuario = user.body();
    return Vue.http.post("http://localhost:5000/receita/resumoReceita", usuario);
  },
  guid: function () {
    function S4() {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
  },

};
